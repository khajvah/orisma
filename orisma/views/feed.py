import json

from pyramid.view import view_config
from zope import component

from orisma.data.debate import IDebateDataAccess
from orisma.views import BaseView


@view_config(route_name='feed', renderer='templates/feed.jinja2')
class Feed(BaseView):
    def __init__(self, request):
        super(Feed, self).__init__(request)
        self.debatesDA = component.getUtility(IDebateDataAccess, 'debate_data_access')

    def get(self):
        debates = [dict(i) for i in self.debatesDA.get(10, 0)]

        return {
            'debates': debates,
            'debates_data': json.dumps(debates),
            'user': self.user
        }
