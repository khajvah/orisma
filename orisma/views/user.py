import colander
from pyramid import exceptions
from pyramid.response import Response
from pyramid.view import view_config
from zope.component import getUtility

from orisma.data.user import IUserDataAccess
from orisma.models import main
from orisma.models.session import ISessionFactory
from orisma import schemas
from orisma.views import BaseView, AuthenticatedView
from zope import component


@view_config(route_name="get_user", renderer="json")
class UserView(BaseView):
    def __init__(self, request):
        super().__init__(request)
        self.dbsession = getUtility(ISessionFactory, 'dbsession_factory').get_session()

    def get(self):
        session = self.dbsession
        obj = session.query(main.User).get(self.request.matchdict["id"])

        if obj is None:
            return exceptions.HTTPNotFound()
        return obj


@view_config(route_name="get_or_create_user", renderer="json")
class UserCreateView(BaseView):
    def __init__(self, request):
        super(UserCreateView, self).__init__(request)
        self.userDA = component.getUtility(IUserDataAccess, 'user_data_access')

    def post(self):
        self.request.response.status = 201
        data = schemas.User().deserialize(self.request.json_body)
        user = self.userDA.create_user(**data)

        return user


@view_config(route_name='login_view', renderer="templates/login.jinja2")
class UserLogin(BaseView):
    def __init__(self, request):
        super(UserLogin, self).__init__(request)

    def get(self):
        return {
            'user': self.user
        }


@view_config(route_name='logout_view', renderer='json')
class UserLogout(AuthenticatedView):
    def __init__(self, request):
        super(UserLogout, self).__init__(request)

    def get(self):
        self.request.session['user'] = None

        return {}


@view_config(route_name='login', renderer='json')
class UserLoginPost(BaseView):
    def __init__(self, request):
        super(UserLoginPost, self).__init__(request)
        self.userDA = component.getUtility(IUserDataAccess, 'user_data_access')

    def post(self):
        session = self.request.session

        user = self.userDA.get_user_by_username_password(self.request.json['username'], self.request.json['password'])
        if user is not None:
            session['user'] = user
            return user

        self.request.response.status = 401
        return


@view_config(route_name='register', renderer='templates/register.jinja2')
class UserRegister(BaseView):
    def __init__(self, request):
        super(UserRegister, self).__init__(request)

    def get(self):
        return {}
