from pyramid import exceptions
from pyramid.view import view_config
from zope import component

from orisma.data.argument import IArgumentDataAccess
from orisma.schemas import Argument
from orisma.views import BaseView, AuthenticatedView


@view_config(route_name="argument_by_id", renderer='json')
class ArgumentById(BaseView):
    def __init__(self, request):
        super().__init__(request)
        self.request = request
        self.argument_DA = component.getUtility(IArgumentDataAccess, 'argument_data_access')

    def get(self):
        argument = self.argument_DA.get(self.request.matchdict['id'])
        if argument is None:
            return exceptions.HTTPNotFound()
        return argument


@view_config(route_name="create_argument", renderer='json')
class CreateArgument(AuthenticatedView):
    def __init__(self, request):
        super().__init__(request)
        self.request = request
        self.argument_DA = component.getUtility(IArgumentDataAccess, 'argument_data_access')

    def post(self):
        self.request.status = 201
        checked_data = Argument().deserialize(self.request.json_body)
        checked_data['user'] = self.request.session['user'].id

        return self.argument_DA.create(**checked_data)
