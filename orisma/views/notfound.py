from pyramid.view import notfound_view_config
from pyramid.response import Response


@notfound_view_config()
def notfound_view(request):
    return Response(status=404)
