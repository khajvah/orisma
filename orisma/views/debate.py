import json
from pyramid import exceptions
from pyramid.view import view_config
from zope import component

from orisma.data.debate import IDebateDataAccess
from orisma.schemas import Debate
from orisma.views import BaseView, AuthenticatedView


@view_config(route_name="debate_by_id", renderer='json')
class DebateById(AuthenticatedView):
    def __init__(self, request):
        super(DebateById, self).__init__(request)
        self.debateDA = component.getUtility(IDebateDataAccess, 'debate_data_access')

    def get(self):
        debate = self.debateDA.get_by_id(self.request.matchdict["id"])
        if debate is None:
            return exceptions.HTTPNotFound()

        return debate


@view_config(route_name="create_debate", renderer='json')
class CreateDebate(AuthenticatedView):
    def __init__(self, request):
        super(CreateDebate, self).__init__(request)
        self.debateDA = component.getUtility(IDebateDataAccess, 'debate_data_access')

    def post(self):
        self.request.status = 201
        checked_data = Debate().deserialize(self.request.json_body)
        checked_data['creator'] = self.request.session['user'].id

        return self.debateDA.create(**checked_data)


@view_config(route_name='debate_view', renderer='templates/debate.jinja2')
class DebateView(BaseView):
    def __init__(self, request):
        super(DebateView, self).__init__(request)
        self.debateDA = component.getUtility(IDebateDataAccess, 'debate_data_access')

    def get(self):
        debate = self.debateDA.get_by_id_with_arguments(self.request.matchdict['id'])
        if debate['debate'] is None:
            raise exceptions.HTTPNotFound()

        debate['arguments'] = list(debate['arguments'])
        debate['debate'] = dict(debate['debate'])

        debate['debate_data'] = json.dumps(debate)
        debate['user'] = self.user
        return dict(debate)
