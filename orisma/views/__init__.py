from colander import Invalid
from pyramid import exceptions
from pyramid.response import Response
from pyramid.view import exception_view_config
from sqlalchemy.exc import IntegrityError


class BaseView:
    def __init__(self, request):
        self.request = request
        self.user = self.request.session.get('user')

    def __call__(self, *args, **kwargs):
        if self.request.method == "GET":
            return self.get()
        elif self.request.method == "POST":
            return self.post()
        elif self.request.method == "DELETE":
            return self.delete()
        elif self.request.method == "PUT":
            return self.put()
        elif self.request.method == "OPTIONS":
            return self.option()

    def get(self):
        return exceptions.HTTPNotFound()

    def post(self):
        return exceptions.HTTPNotFound()

    def option(self):
        return exceptions.HTTPNotFound()

    def delete(self):
        return exceptions.HTTPNotFound()

    def put(self):
        return exceptions.HTTPNotFound()


class AuthenticatedView(BaseView):
    def __init__(self, request):
        super(AuthenticatedView, self).__init__(request)

    def __call__(self, *args, **kwargs):
        if not self.request.session.get('user'):
            return exceptions.HTTPForbidden()
        super(AuthenticatedView, self).__call__(args, kwargs)


@exception_view_config(Exception)
def server_error(exc, request):
    return Response(str(exc), status=500)


@exception_view_config(Invalid)
def invalid_data_error(exc, request):
    return Response(str(exc), status=400)


@exception_view_config(IntegrityError)
def database_integrity_error(exc, request):
    return Response("DB error: " + str(exc), status=400)
