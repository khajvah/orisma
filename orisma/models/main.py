import datetime

from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
    String,
    ForeignKey,
    Boolean,
    DateTime
)

from .meta import Base


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String)
    fullname = Column(String)
    email = Column(String)
    password = Column(String)
    is_verified = Column(Boolean, default=False)
    created = Column(DateTime, default=datetime.datetime.now())

    def __json__(self, request):
        return {
            "id": self.id,
            "username": self.username,
            "fullname": self.fullname,
            "email": self.email
        }


class Debate(Base):
    __tablename__ = 'debates'
    id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(Text)
    description = Column(Text)
    creator = Column(Integer, ForeignKey('users.id'))
    created = Column(DateTime, default=datetime.datetime.now())

    def __json__(self, request):
        return {
            "id": self.id,
            "title": self.title,
            "description": self.description,
            "creator": self.creator,
        }

    def __iter__(self):
        yield 'id', self.id
        yield 'title', self.title
        yield 'description', self.description
        yield 'creator', self.creator


class Argument(Base):
    __tablename__ = 'arguments'
    id = Column(Integer, primary_key=True, autoincrement=True)
    body = Column(Text)
    user = Column(Integer, ForeignKey('users.id'))
    created = Column(DateTime, default=datetime.datetime.now())

    def __json__(self, request):
        return {
            "id": self.id,
            "body": self.body,
            "user": self.user
        }

    def __iter__(self):
        yield "id", self.id
        yield "body", self.body
        yield "user", self.user


class ArgumentConnection(Base):
    __tablename__ = 'arguments_connections'
    id = Column(Integer, primary_key=True, autoincrement=True)
    debate = Column(Integer, ForeignKey('debates.id'), nullable=False)
    parent = Column(Integer, ForeignKey('arguments.id'))
    child = Column(Integer, ForeignKey('arguments.id'), nullable=False)
    position = Column(Text, nullable=False)

    def __json__(self, request):
        return {
            'id': self.id,
            'debate': self.debate,
            'parent': self.parent,
            'child': self.child,
            'position': self.position
        }

    def __iter__(self):
        yield "id", self.id
        yield "debate", self.debate
        yield "parent", self.parent
        yield "child", self.child
        yield "position", self.position

