from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker
from zope import interface
from zope.interface import implementer


class ISessionFactory(interface.Interface):
    def get_session(self):
        pass


@implementer(ISessionFactory)
class SQLAlchemySessionFactory:

    def __init__(self, settings, prefix='sqlalchemy.'):
        engine = engine_from_config(settings, prefix)
        self.factory = sessionmaker()
        self.factory.configure(bind=engine)

    def get_session(self):
        return self.factory()
