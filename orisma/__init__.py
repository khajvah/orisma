from pyramid.config import Configurator
from zope.component import getGlobalSiteManager


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    globalreg = getGlobalSiteManager()
    config = Configurator(registry=globalreg)
    config.setup_registry(settings=settings)
    config.include('.models')
    config.include('.routes')
    config.include('pyramid_jinja2')
    config.include('pyramid_redis_sessions')
    config.add_static_view(name='assets', path='public/assets')
    config.scan()
    config.commit()
    # config.include('pyramid_jwt')
    # config.set_authentication_policy(ACLAuthorizationPolicy())
    # config.set_jwt_authentication_policy('secret')
    return config.make_wsgi_app()
