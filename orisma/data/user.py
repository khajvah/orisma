import bcrypt
from sqlalchemy.exc import DatabaseError

from zope import interface
from zope.interface import implementer
from zope.component import getUtility

from orisma.models.session import ISessionFactory
from orisma.models.main import User


class IUserDataAccess(interface.Interface):

    def get_user_by_id(self):
        pass

    def create_user(self):
        pass


@implementer(IUserDataAccess)
class UserDataAccess:

    def __init__(self):
        """
        """
        self.dbsession = getUtility(ISessionFactory, 'dbsession_factory').get_session()

    def get_user_by_id(self, user_id: int) -> User:
        return self.dbsession.query(User).get(user_id)

    def get_user_by_username_password(self, username: str, password: str):
        user = self.dbsession.query(User).filter(User.username == username)
        if bcrypt.checkpw(password.encode(), user[0].password.encode()):
            return user[0]
        return None

    def create_user(self, username, email, password, fullname=None):
        hashed_password = bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode()
        new_user = User(username=username, fullname=fullname, email=email, password=hashed_password)
        try:
            self.dbsession.add(new_user)
            self.dbsession.commit()
        except DatabaseError as e:
            self.dbsession.rollback()
            raise e
        return new_user
