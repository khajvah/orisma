from sqlalchemy.exc import DatabaseError
from zope.component import getUtility
from zope.interface import Interface, implementer

from orisma.models.main import Argument, ArgumentConnection
from orisma.models.session import ISessionFactory


class IArgumentDataAccess(Interface):
    def get_by_id(self, argument_id):
        pass

    def create(self):
        pass


@implementer(IArgumentDataAccess)
class SQLArgumentDataAccess:
    def __init__(self):
        self.db_session = getUtility(ISessionFactory, 'dbsession_factory').get_session()

    def get_by_id(self, argument_id: int) -> Argument:
        pass

    def create(self, debate: int, user: int, body: str, parent: int, position: str):
        try:
            new_argument = Argument(user=user, body=body)
            self.db_session.add(new_argument)
            self.db_session.flush()
            argument_connection = ArgumentConnection(debate=debate, parent=parent, child=new_argument.id, position=position)

            self.db_session.add(argument_connection)
            self.db_session.commit()

            return new_argument
        except DatabaseError as e:
            self.db_session.rollback()
            raise e
