from sqlalchemy.exc import DatabaseError
from zope.component import getUtility
from zope.interface import Interface, implementer

from orisma.models.session import ISessionFactory
from orisma.models.main import *


class IDebateDataAccess(Interface):
    def get_by_id(self):
        pass

    def create(self, title: str, description: str, creator: int) -> Debate:
        """
        Create a new Debate
        :param title: str
        :param description: str
        :param creator: int
        :return:
        """
        pass


@implementer(IDebateDataAccess)
class SQLDebateDataAccess:
    def __init__(self):
        self.db_session = getUtility(ISessionFactory, 'dbsession_factory').get_session()

    def get_by_id(self, debate_id: int) -> Debate:
        """
        Get debate by id

        :param debate_id:
        :return: Debate
        """

        return self.db_session.query(Debate).get(debate_id)

    def get(self, limit, offset, sort='created'):
        """
        Get debates.

        :param limit:
        :param offset:
        :param sort: how to sort. defaults to created date.
        :return:
        """

        return self.db_session.query(Debate).order_by(Debate.created).limit(limit).offset(offset)

    def get_by_id_with_arguments(self, debate_id: int):
        """
        Get Debate by id and include arguments
        :param debate_id:
        :return:
        """
        res = {
            "debate": self.db_session.query(Debate).get(debate_id),
            "arguments": self.db_session.query(ArgumentConnection.position, Argument.body, Argument.user, Argument.id)
            .join(Argument, Argument.id == ArgumentConnection.child).filter(ArgumentConnection.debate == debate_id)
        }

        return res

    def create(self, title: str, description: str, creator: int) -> Debate:
        """
        Create a new debate.

        :param title:
        :param description:
        :param creator:
        :return:
        """

        new_debate = Debate(title=title, description=description, creator=creator)

        self.db_session.add(new_debate)
        try:
            self.db_session.commit()
        except DatabaseError as e:
            self.db_session.rollback()
            raise e

        self.db_session.refresh(new_debate)
        return new_debate
