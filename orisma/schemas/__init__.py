import colander


class User(colander.MappingSchema):
    username = colander.SchemaNode(colander.String())
    fullname = colander.SchemaNode(colander.String(), missing=colander.drop)
    password = colander.SchemaNode(colander.String())
    email = colander.SchemaNode(colander.String(), validator=colander.Email())


class Debate(colander.MappingSchema):
    title = colander.SchemaNode(colander.String())
    description = colander.SchemaNode(colander.String())


class Argument(colander.MappingSchema):
    debate = colander.SchemaNode(colander.Integer())
    body = colander.SchemaNode(colander.String())
    parent = colander.SchemaNode(colander.Integer(), missing=None)
    position = colander.SchemaNode(colander.String())
