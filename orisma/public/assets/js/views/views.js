var Argument = Backbone.View.extend({
    events: {
        'click': 'testClick'
    },

    testClick: function () {
        alert('clicked ' + this.model[3]);
    }
});

var Debate = Backbone.View.extend({
    initialize: function (debate) {
        this.arguments = [];
        var that = this;
        debate.arguments.forEach(function (argument) {
            that.arguments.push(new Argument({model: argument, el: '#argument' + argument[3]}))
        })
    }
});